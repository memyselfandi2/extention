/**
 * (c) Heidar Mostafa
 */

(function() {
    (function() {
        var carbColIndex = 3;
        var tds = $('#diary-table tr:not([class^="total"]) td:nth-child(' + carbColIndex + ')').has('.macro-value').filter(function() {
            return $(this).text().trim();
        });

        var sumNetCarb = 0;
        var allNetCarbs = 0;
        for (var i = 0; i < tds.length; i++) {
            var td = tds[i];
            $(td).css({
                background: '#c00',
                color: '#fff'
            });
            $(td).next().next().next().css({
                background: '#c00',
                color: '#fff'
            });
            var carbVal = Number($(td).children('.macro-value').text());
            var fiberVal = Number($(td).next().next().next().text());
            var netCarbs = !isNaN(carbVal - fiberVal) && (carbVal - fiberVal) > 0 ? carbVal - fiberVal : 0;
            if ($(td).parent().hasClass('bottom')) {
                $('<td style="color:#fff; background:#0c0"><span class="macro-value">' + sumNetCarb + '</span></td>').insertAfter(td);
                sumNetCarb = 0;
            } else {
                $('<td style="color:#fff; background:#0c0"><span class="macro-value">' + netCarbs + '</span></td>').insertAfter(td);
                sumNetCarb += netCarbs;
                allNetCarbs += netCarbs;
            }
        }
        $('.total:first td:nth-child(' + (carbColIndex) + ') .macro-value').text(allNetCarbs);
        $('.total.remaining td:nth-child(' + (carbColIndex) + ') .macro-value').text(allNetCarbs - Number($('.total.alt td:nth-child(' + (carbColIndex) + ') .macro-value').text()));
        $('<td class="alt nutrient-column">Net Carbs<div class="subtitle">g</div></td>').insertAfter('.meal_header td:nth-child(' + carbColIndex + ')');
        $('tfoot .nutrient-column:nth-child(' + (carbColIndex) + ')').html('Net Carbs<div class="subtitle">g</div>');
        $('.macro-percentage').each(function() {
            $(this).text().trim() && !isNaN(Number($(this).text())) ? $(this).css({
                display: 'block',
                color: '#333',
                'font-size': '11px',
                background: '#ccc'
            }).append('%') : null;
        });
    })()
})();