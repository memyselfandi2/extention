/**
 * Created by Tibor Helmig on 01.03.17.
 * (c) softgames GmbH
 */
(function() {
    (function () {
        var ingredients = [];
        $( '.food-list .all_matched').each( function( key, item ){
            var nutritions = $( item ).find( '.nutrition');
            var calories = $( nutritions[ 0 ] ).find( '.calories' ).text().trim();
            var fat = $( nutritions[ 0 ] ).find( '.fat' ).text().trim();
            var carbs = $( nutritions[ 0 ] ).find( '.carbs' ).text().trim();
            var protein = $( nutritions[ 0 ] ).find( '.protein' ).text().trim();

            ingredients.push( {
                ingredient: $( item ).attr( 'data-ingredient' ),
                version: $( item ).attr( 'data-version' ),
                quantity: $( item ).attr( 'data-quantity' ),
                weight: $( item ).attr( 'data-weight' ),
                description: $( item ).attr( 'data-description' ),
                nutrition: {
                    calories: calories,
                    fat: fat,
                    carbs: carbs,
                    protein: protein
                }
            } );
        });

        // grab username
        var username = $( $( '.user-2' )[ 0 ] ).text();

        // grap recipe name
        var name = $( 'input[ name = title ]' ).val();

        // grab serving
        var servings = $( 'input[ name = recipe_servings ]' ).val();

        // grep total calories
        var totalCalories = $( $( '.calories .num' )[ 0 ] ).text().trim();

        // greb the recpie id
        var pathnames = window.location.pathname.split( '/' );
        var recipeId = pathnames[ pathnames.length - 1 ];

        // generate json
        var postJson= {
            myfitnesspalRecId: recipeId,
            name: name,
            servings: servings,
            username: username,
            totalCalories: totalCalories,
            ingredients: ingredients
        };

        // get fiber to calc net carbs!
        $.ajax({
            url: 'http://www.myfitnesspal.com/recipe/view/' + recipeId,
            type: 'GET',
            success: function(data){
                //On ajax success do this
                var fiberValue = $( $(data).find( '#fiber' ).find( '.value' )[ 0 ] ).text();
                postJson.fiber = fiberValue;

                $.ajax({
                    //url: 'http://127.0.0.1:1234/api/recipe',
                    url: 'http://food.helmig.berlin/api/recipe',
                    type: 'POST',
                    contentType:'application/json',
                    data: JSON.stringify( postJson ),
                    dataType:'json',
                    success: function(data){
                        //On ajax success do this
                        console.log(data);
                    },
                    error: function( xhr, ajaxOptions, thrownError ){
                        //On error do this
                        if( xhr.status == 200 ){

                            console.log( ajaxOptions );
                        }
                        else{
                            console.log( xhr.status );
                            console.log( thrownError );
                        }
                    }
                });
            },
            error: function( xhr, ajaxOptions, thrownError ){
                //On error do this
                if( xhr.status == 200 ){

                    console.log( ajaxOptions );
                }
                else{
                    console.log( xhr.status );
                    console.log( thrownError );
                }
            }
        });
    })();
})();