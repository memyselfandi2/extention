/**
 * Created by Tibor Helmig on 01.03.17.
 * (c) Tibor Helmig
 */
var jqueryIncluded = false;

document.addEventListener('DOMContentLoaded', function() {
    var importRecipeBtn = document.getElementById('importRecipe');
    var exportRecipeBtn = document.getElementById('exportRecipe');
    var calcNetCarbsBtn = document.getElementById('calcNetCarbs');

    exportRecipeBtn.addEventListener('click', function() {
        chrome.tabs.getSelected(null, function(tab) {
            // check url
            if( !tab.url.match( '//www.myfitnesspal.com/recipe/edit_v2/' ) ){
                chrome.tabs.executeScript(tab.id, {"code" : 'console.log("Please go to the recipe edit page.");'});
                return;
            }

            if( !jqueryIncluded ){
                chrome.tabs.executeScript(null, {
                    file: "ext/jquery-3.1.1.min.js"
                });
                jqueryIncluded = true;
            }

            chrome.tabs.executeScript(null, {
                file: "js/exportRecipe.js"
            });
        });
    }, false);

    calcNetCarbsBtn.addEventListener('click', function() {
        chrome.tabs.getSelected(null, function(tab) {
            //console.log('foo-bar');
            //chrome.tabs.executeScript(tab.id, {"code" : 'console.log("FOO-BAR");'});

            if( !jqueryIncluded ){
                chrome.tabs.executeScript(null, {
                    file: "ext/jquery-3.1.1.min.js"
                });
                jqueryIncluded = true;
            }

            chrome.tabs.executeScript(null, {
                file: "js/netCarbs.js"
            });
        });
    }, false);
}, false);