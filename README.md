#installation
- go to chrome://extensions
- check the "Developer mode" is active
- click "load unpacked extention..."

#Usage
## export recipe
- go to the recipe edit mode
- press the export recipe button ( You will get no response!, Errors are logged in the console )
- check your recipe at http://food.helmig.berlin

## get splitted carbs
- go to your Food Diary
- press the calc carbs button from the extention

## import recipe
- go to the detail view of the recipe you want
- click the "copy url for myfitnesspal" button
- go to myfitnesspal recipes page
- past ( cmd + v | crtl + v ) the Recipe Importer